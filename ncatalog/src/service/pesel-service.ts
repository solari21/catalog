export interface PESELService {
    verify(pesel: string): boolean;
}