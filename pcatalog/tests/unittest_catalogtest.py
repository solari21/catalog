import unittest
from unittest.mock import MagicMock

from catalog.catalog import Catalog
from catalog.db import DB
from catalog.person import Person


class CatalogTest(unittest.TestCase):

    def test_should_get_person_by_pesel(self):
        # given
        p = Person("imie", "nazwisko", "pesel")
        mock = DB()  # stworzenie obiektu ktory bedzie mockiem, zero magii
        mock.get_person = MagicMock(return_value=p)  # szykujemy zachowanie jednej metody, to jest mock
        c = Catalog(mock, None)  # tworzymy testowany obiekt, spelniajac zaleznosc mockiem

        # when
        result = c.get_person("pesel")  # wlasciwy test

        # then
        self.assertEqual(p, result)  # weryfikacja po tescie
        mock.get_person.assert_called_with("pesel")  # dodatkowo weryfikujemy obiekt zastepczy

        # Mozna tez tak, zdefiniowac liste wolan i potem weryfikowac cala liste:
        # calls = [call("pesel")];
        # mock.get_person.assert_has_calls(calls, any_order=False)
