package pl.poznan.put.spio.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.poznan.put.spio.service.Catalog;

@AllArgsConstructor
@RestController
public class CatalogController {

    private Catalog catalog;

    @GetMapping("/pesel/{name},{surname}")
    public String getPesel(@PathVariable final String name,
                           @PathVariable final String surname) {
        return catalog.getPESEL(name, surname);
    }

    // normalnie taka operacja to będzie POST/PUT, ale dla wygody zabawy robimy mapowanie GET
    @GetMapping("/person/{name},{surname},{pesel}")
    public void addPerson(@PathVariable final String name,
                          @PathVariable final String surname,
                          @PathVariable final String pesel) {
        catalog.addPerson(name, surname, pesel);
    }
}
